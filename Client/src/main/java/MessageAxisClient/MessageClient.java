package MessageAxisClient;

import UserAxisClient.UserServiceStub;
import org.apache.axis2.AxisFault;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

public class MessageClient {

    private static MessageServiceStub messageServiceStub;

    static {
        try {
            messageServiceStub = new MessageServiceStub();
        } catch (AxisFault axisFault) {
            axisFault.printStackTrace();
        }
    }

    public static List<MessageServiceStub.Message> getAllMessages() throws RemoteException {

        MessageServiceStub.GetAllMessages req = new MessageServiceStub.GetAllMessages();

        MessageServiceStub.GetAllMessagesResponse resp = messageServiceStub.getAllMessages(req);

        MessageServiceStub.Message[] messagesArray = resp.get_return();

        List<MessageServiceStub.Message> messages = Arrays.asList(messagesArray);

        return messages;
    }


    public static List<MessageServiceStub.Message> getMessagesOfUser(String username) throws RemoteException {

        MessageServiceStub.GetMessagesOfUser req = new MessageServiceStub.GetMessagesOfUser();
        //On set le paramètre de requete nous permettant d'avoir le message de l'utilisateur
        req.setUserName(username);

        MessageServiceStub.GetMessagesOfUserResponse resp = messageServiceStub.getMessagesOfUser(req);

        MessageServiceStub.Message[] messagesOfUserArray = resp.get_return();

        List<MessageServiceStub.Message> messagesOfUser = Arrays.asList(messagesOfUserArray);

        return messagesOfUser;
    }

    public static MessageServiceStub.Message updateMessage(int idMessage, MessageServiceStub.Message newData) throws RemoteException {

        MessageServiceStub.UpdateMessage req = new MessageServiceStub.UpdateMessage();
        req.setIdMessage(idMessage);
        req.setNewData(newData);

        MessageServiceStub.UpdateMessageResponse resp = messageServiceStub.updateMessage(req);

        MessageServiceStub.Message message = resp.get_return();

        return message;

    }

    public static void addMessage(MessageServiceStub.Message message) throws RemoteException{
        MessageServiceStub.AddMessage req = new MessageServiceStub.AddMessage();
        req.setMessage(message);
        //Ajout dans la bdd via le service
        messageServiceStub.addMessage(req);
    }

    public static void deleteMessage(int idMessage) throws RemoteException {
        MessageServiceStub.DeleteMessage req = new MessageServiceStub.DeleteMessage();
        req.setIdMessage(idMessage);

        //Delete
        messageServiceStub.deleteMessage(req);
    }



    public static MessageServiceStub.User toMsgUser(UserServiceStub.User user){
        MessageServiceStub.User userMsg = new MessageServiceStub.User();
        userMsg.setId(user.getId());
        userMsg.setFullName(user.getFullName());
        userMsg.setPassword(user.getPassword());
        userMsg.setUsername(user.getUsername());
        return userMsg;
    }


    public static MessageServiceStub.Message getMessage(int idMessage) throws RemoteException {
        MessageServiceStub.GetMessage req = new MessageServiceStub.GetMessage();
        req.setIdMessage(idMessage);

        //Getters du Msg
        return  messageServiceStub.getMessage(req).get_return();

    }
}
