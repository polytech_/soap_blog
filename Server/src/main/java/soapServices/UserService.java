package soapServices;

import dao.UserDao;
import model.User;
import utils.HibernateUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;



public class UserService {
    private final UserDao userDao = new UserDao(HibernateUtil.getSession());


    public List<User> allUsers(){
        return userDao.getAllUsers();
    }


    public User getUserByCredentials(String userName, String password) {
        return userDao.getUserByCredentials(userName, password).get();
    }


}
