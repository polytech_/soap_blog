package controller;

import MessageAxisClient.MessageClient;
import MessageAxisClient.MessageServiceStub;
import UserAxisClient.UserClient;
import UserAxisClient.UserServiceStub;
import model.Message;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ModifyMerssageController",
            urlPatterns = "/modify")
public class ModifyMessageController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Récupération des données relatives au message
        int messageId = Integer.parseInt(request.getParameter("messageId"));
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String messageText = request.getParameter("messageText");

        //Récupération de l'objet utilisateur du posteur du message
        UserServiceStub.User poster = UserClient.getUserByCredentials(username, password);

        //Récupération de l'objet message à partir des données précédentes
        MessageServiceStub.Message newData =
                new MessageServiceStub.Message();


        newData.setMessageText(messageText);
        newData.setMessagePoster(MessageClient.toMsgUser(poster));

        //Transmission des données au service REST afin d'effectuer la mise à jour dans la bdd
        MessageClient.updateMessage(messageId, newData);

        //Retour vers la page et confirmation
        request.setAttribute("confirmModify", "Le message a bien été modifié !");

        request.getRequestDispatcher("mymessages").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
