package controller;

import MessageAxisClient.MessageClient;
import MessageAxisClient.MessageServiceStub;
import UserAxisClient.UserClient;
import UserAxisClient.UserServiceStub;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "addMessageController",
            urlPatterns = "/addMessage")
public class addMessageController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Récupération des données relatives au message
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String messageText = request.getParameter("messageText");

        //Récupération de l'objet utilisateur du posteur du message
        UserServiceStub.User poster = UserClient.getUserByCredentials(username, password);

        //Création de l'objet message à partir des données précédentes
        MessageServiceStub.Message messageToAdd = new MessageServiceStub.Message();
        messageToAdd.setMessageText(messageText);
        messageToAdd.setMessagePoster(MessageClient.toMsgUser(poster));

        //Ajout du message à partir du client consommant le service REST des messages
        MessageClient.addMessage(messageToAdd);

        //Transmission des données au service REST afin d'effectuer la mise à jour dans la bdd
        request.setAttribute("confirmPost", "Le message a bien été posté !");

        request.getRequestDispatcher("mymessages").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
