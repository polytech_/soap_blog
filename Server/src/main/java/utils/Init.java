package utils;

import dao.MessageDao;
import dao.UserDao;
import model.Message;
import model.User;
import org.hibernate.Session;

public class Init {

    public static void main(String[] args) {
        Session hibernateSession = HibernateUtil.getSession();
        UserDao userDao = new UserDao(hibernateSession);
        MessageDao messageDao = new MessageDao(hibernateSession);

        if(userDao.getAllUsers().isEmpty()) {
            userDao.addUser(new User("othmane97", "mdp1", "Othmane Allamou"));
            userDao.addUser(new User("nour97", "mdp2", "Nour Nasrallah"));
            userDao.addUser(new User("yang97", "mdp3", "Yang Yang"));
            userDao.addUser(new User("tariq96", "mdp4", "Tariq Chaairat"));
        }

        if(messageDao.getAllMessages().isEmpty()) {

            Message message1 = new Message("laudantium enim quasi est quidem magnam voluptate", userDao.getUserByUsername("othmane97").get());
            Message message2 = new Message("est natus enim nihil est dolore omnis voluptatem numquam ", userDao.getUserByUsername("nour97").get());
            Message message3 = new Message("harum non quasi et ratione ntempore iure ex voluptates ", userDao.getUserByUsername("yang97").get());
            Message message4 = new Message("ut voluptatem corrupti velit nad voluptatem maiores net",userDao.getUserByUsername("tariq96").get());
            Message message5 = new Message("expedita maiores dignissimos facilis nipsum est rem est fugit", userDao.getUserByUsername("othmane97").get());
            Message message6 = new Message("nihil ut voluptates blanditiis autem odio dicta rerum nquisquam", userDao.getUserByUsername("nour97").get());
            Message message7 = new Message("veritatis voluptates necessitatibus maiores corrupti neque", userDao.getUserByUsername("yang97").get());
            Message message8 = new Message("deleniti aut sed molestias explicabo ncommodi odio ratione", userDao.getUserByUsername("tariq96").get());

            messageDao.addMessage(message1);
            messageDao.addMessage(message2);
            messageDao.addMessage(message3);
            messageDao.addMessage(message4);
            messageDao.addMessage(message5);
            messageDao.addMessage(message6);
            messageDao.addMessage(message7);
            messageDao.addMessage(message8);
        }
    }
}
