package soapServices;


import dao.MessageDao;
import model.Message;
import utils.HibernateUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;



public class MessageService {
    private final MessageDao messageDao = new MessageDao(HibernateUtil.getSession());



    public List <Message> GetAllMessages() {
        return messageDao.getAllMessages();
    }



    public List<Message> getMessagesOfUser(String userName) {
        return messageDao.getAllMessagesByUsername(userName);
    }



    public Message getMessage(int idMessage){
        return messageDao.getMessage(idMessage).get();
    }



    public Message addMessage(Message message) {
        return messageDao.addMessage(message);
    }



    public Message updateMessage(int idMessage, Message newData) {
        newData.setId(idMessage);
        return messageDao.updateMessage(idMessage, newData);
    }


    public void deleteMessage(int idMessage) {
        messageDao.deleteMessage(idMessage);
    }


}
