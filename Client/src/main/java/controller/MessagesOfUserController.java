package controller;

import MessageAxisClient.MessageClient;
import MessageAxisClient.MessageServiceStub;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "MessagesOfUserController",
            urlPatterns = "/messagesOfUser")
public class MessagesOfUserController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");

        if(username.equals("-1")) {
            request.setAttribute("error", "Merci de sélectionner le nom d'un utilisateur");
            request.getRequestDispatcher("messages").forward(request, response);
            return;
        }

        List < MessageServiceStub.Message> messages = MessageClient.getMessagesOfUser(username);

        //On mets la listes des messages de l'utilisateur connectés dans la requête
        request.setAttribute("messages", messages);



        request.getRequestDispatcher("viewMessage.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
