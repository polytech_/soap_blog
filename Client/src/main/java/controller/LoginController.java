package controller;

import UserAxisClient.UserClient;
import UserAxisClient.UserServiceStub;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginController",
            urlPatterns = "/login")
public class LoginController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        UserServiceStub stub = new UserServiceStub();

        UserServiceStub.User user = UserClient.getUserByCredentials(username, password);

        String ids = String.join("/", username, password);

        //Création de la session et initialisation des identifiants
        request.getSession().setAttribute("ids", ids);
        //Session valide pendant 1 minute
        request.getSession().setMaxInactiveInterval(60);

        //request.getRequestDispatcher("messages").forward(request, response);

        response.sendRedirect("messages");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
