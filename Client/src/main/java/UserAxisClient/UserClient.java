package UserAxisClient;

import org.apache.axis2.AxisFault;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

public class UserClient {

    private static UserServiceStub userServiceStub;

    static {
        try {
            userServiceStub = new UserServiceStub();
        } catch (AxisFault axisFault) {
            axisFault.printStackTrace();
        }
    }


    public static UserServiceStub.User getUserByCredentials(String username, String password) throws RemoteException {

        UserServiceStub.GetUserByCredentials request = new UserServiceStub.GetUserByCredentials();

        //On set les paramètres pour get le bon utilisateur
        request.setUserName(username);
        request.setPassword(password);

        UserServiceStub.GetUserByCredentialsResponse resp = userServiceStub.getUserByCredentials(request);

        UserServiceStub.User user = resp.get_return();

        return user;
    }


    public static List<UserServiceStub.User> getAllUsers() throws RemoteException {

        UserServiceStub.AllUsers req = new UserServiceStub.AllUsers();

        UserServiceStub.AllUsersResponse resp = userServiceStub.allUsers(req);

        UserServiceStub.User[] usersArray = resp.get_return();

        List<UserServiceStub.User> users = Arrays.asList(usersArray);

        return users;
    }
}
