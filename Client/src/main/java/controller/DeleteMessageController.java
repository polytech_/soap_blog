package controller;

import MessageAxisClient.MessageClient;
import MessageAxisClient.MessageServiceStub;
import model.Message;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteMessageController",
            urlPatterns = "/delete")
public class DeleteMessageController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Récupération des données relatives au message
        MessageServiceStub.Message messageToDelete = (MessageServiceStub.Message)request.getAttribute("messageToDelete");

        MessageClient.deleteMessage(messageToDelete.getId());

        //Retour vers la page et confirmation
        request.setAttribute("confirmDelete", "Le message a bien été supprimé !");

        request.getRequestDispatcher("mymessages").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
