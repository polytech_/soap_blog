/**
 * MessageServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package MessageAxisClient;


/**
 *  MessageServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class MessageServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public MessageServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public MessageServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for getAllMessages method
     * override this method for handling normal response from getAllMessages operation
     */
    public void receiveResultgetAllMessages(
        MessageAxisClient.MessageServiceStub.GetAllMessagesResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAllMessages operation
     */
    public void receiveErrorgetAllMessages(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getMessage method
     * override this method for handling normal response from getMessage operation
     */
    public void receiveResultgetMessage(
        MessageAxisClient.MessageServiceStub.GetMessageResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getMessage operation
     */
    public void receiveErrorgetMessage(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for deleteMessage method
     * override this method for handling normal response from deleteMessage operation
     */
    public void receiveResultdeleteMessage() {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from deleteMessage operation
     */
    public void receiveErrordeleteMessage(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for addMessage method
     * override this method for handling normal response from addMessage operation
     */
    public void receiveResultaddMessage(
        MessageAxisClient.MessageServiceStub.AddMessageResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from addMessage operation
     */
    public void receiveErroraddMessage(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for updateMessage method
     * override this method for handling normal response from updateMessage operation
     */
    public void receiveResultupdateMessage(
        MessageAxisClient.MessageServiceStub.UpdateMessageResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from updateMessage operation
     */
    public void receiveErrorupdateMessage(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getMessagesOfUser method
     * override this method for handling normal response from getMessagesOfUser operation
     */
    public void receiveResultgetMessagesOfUser(
        MessageAxisClient.MessageServiceStub.GetMessagesOfUserResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getMessagesOfUser operation
     */
    public void receiveErrorgetMessagesOfUser(java.lang.Exception e) {
    }
}
