package controller;

import MessageAxisClient.MessageClient;
import MessageAxisClient.MessageServiceStub;
import UserAxisClient.UserClient;
import UserAxisClient.UserServiceStub;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "MessagesController",
            urlPatterns = "/messages")
public class

  MessagesController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      String action = request.getParameter("action");

      //Si c'est un retour d'erreur de la page des messages on recharge la page avec doGet
      if(action==null) {
        doGet(request, response);
      }

      else {

        if(action.equals("modify")) {
          //Récupération de l'id du message à modifier
          int idMessage = Integer.parseInt(request.getParameter("messageId"));
          //Récupération de l'objet grâce au client SOAPqui consomme le service des messages
          MessageServiceStub.Message messageToModify = MessageClient.getMessage(idMessage);
          //On met le message à modifier dans la requete
          request.setAttribute("messageToModify", messageToModify);
          //On redirige vers la page de modification
          request.getRequestDispatcher("modifyMessage.jsp").forward(request, response);
        }
        else if (action.equals("delete")) {
          //Récupération de l'id du message à supprimer
          int idMessage = Integer.parseInt(request.getParameter("messageId"));
          //Récupération de l'objet grâce au client REST qui consomme le service des messages
          MessageServiceStub.Message messageToDelete = MessageClient.getMessage(idMessage);
          //On met le message à supprimer dans la requete
          request.setAttribute("messageToDelete", messageToDelete);
          //On redirige vers le controlleur de suppression
          request.getRequestDispatcher("/delete").forward(request, response);
        }

      }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      String action = request.getParameter("action");

      if(action!=null) {
        doPost(request, response);
      }

      else {
        //On récupère la session présente
        HttpSession session = request.getSession(false);

        //On récupère les identifiants présents dans la session
        String ids = (String)session.getAttribute("ids");

        //On récupère la liste des messages
        List<MessageServiceStub.Message> allMessages = MessageClient.getAllMessages();

        //On récupère la liste des utilisateurs pour l'afficher dans le select
        List<UserServiceStub.User> usersList = UserClient.getAllUsers();

        //On exclus de la liste des utilisateurs affichés, l'utilisateur connecté (à voir)
//        for (int index=0; index<usersList.size(); index++) {
//          User userTodelete = usersList.get(index);
//          if (userTodelete.getUsername().equals(ids.split("/")[0])) {
//            usersList.remove(userTodelete);
//          }
//        }

        //On transmets les données à la page pour qu'elle les affiche
        request.setAttribute("allMessages", new ArrayList<>(allMessages));
        request.setAttribute("usersList", new ArrayList<>(usersList));

        request.getRequestDispatcher("messages.jsp").forward(request, response);
      }
    }
}
